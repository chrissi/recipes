# Feigen-Mascarpone

*Dessert, Süß, Vegetarsich*

**4 Portionen**


----
- *4* Feigen
- *250 g* Mascarpone
- *20 g* Zucker + 1 EL
- *2 EL* Grappa + etwas Grappa zum Beträufeln, alternativ heller Traubensaft

----
Den Backofen auf 200 Grad vorheizen.
Die Feigen vorsichtig waschen, danach abtrocknen.
Den Stiel entfernen und die Feigen über Kreuz nicht ganz bis zum Boden
einschneiden. 
Danach vorsichtig auseinanderziehen.
Den Mascarpone mit dem Zucker und dem Grappa cremig rühren. 
Die Feigen mit Zucker bestreuen und mit dem Grappa beträufeln.
In eine Auflaufform setzen und die Feigen im Backofen ca. 10 – 15 Minuten backen.
Den Saft, der dabei entsteht, unter die Mascarpone-Creme ziehen.
Die Creme in eine Form geben, die Feigen darauf setzen und das Dessert,
mindestens 30 Minuten kalt stellen.
