# Lava-Schoko Muffins

*Dessert, Süß, Vegetarisch*

**8 Portionen**


----
- *125 g* Butter in stückchen
- *150 g* feiner Rohrzucker
- *125 g* dunkle Schokolade( mit chilli)
- *3* Eier
- *35 g* Mehl

----
Backofen auf 250 vorheizen, Auflauf Förmchen oder Tulpen-Muffinformen 
mit Butter ausstreichen und mit Zucker bestreuen. 
Butter mit Schoki im Wasserbad schmelzen.
Die Eier mit dem Zucker schaumig schlagen und das Mehl darübersieben und
unterrühren.
Die Schoko Masse darunterziehen und in die Förmchen giessen.
10-12 min .backen dann sofort servieren. 

Dazu passt:

- Tonka-Sahne: Dafür Tonkabohne reiben und mit angeschlagener Sahne vermischen.
- Orangenfilets oder bitter Orangenmarmelade
- Preiselbeeren

