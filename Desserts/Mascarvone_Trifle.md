# MarscarVone Trifle

*Dessert, Glas, vegan*

**4 Personen**

----

- *250 g* MarcarVone
- *50 g* Zucker
- *40 ml* veganer Milchersatz (glutenrei)
- *1* Vanillezucker
- *1 Priese* Zimt
- *150 g* Himbeeren TK
- *150 g* Apfelkompott
- Spekulatiuskekse

----

- MascarVone mit Zucker, Vanillezucker, Zimt und Milchersatz aufschlagen. (Das Ergebnis errinnert sehr an Eischnee.)
- Creme abschmecken.
- Himeeren auftauen und zuckern.
- Spekulatiuskrümel herstellen.
- Fruchtzubereitung unten in ein Glas geben, dann Creme darauf. Kann in diesem Zustand kühl gestellt werden.
- Spekulatiuskrümel vor dem Servieren dazu geben.