# Mousse au Chocolat

*Dessert, Süß, Vegetarisch*

**4-6 Portionen**


----
- *2 Tafeln* Zartbitterschokolade à 100g
- *3* Eier
- *250 ml* Sahne
- *40 g* Zucker
- *2 EL* heißes Wasser

----
Die Eier sorgfältig trennen.
Das Eiweiß und die Sahne – getrennt – sehr steif schlagen und beides in den
Kühlschrank stellen.

Die Schokolade im Wasserbad schmelzen.

Das Eigelb zusammen mit 2 Esslöffeln heißem Wasser
(max. Leitungswassertemperatur reicht, ca. 50-60°C) 
und den Zucker in eine Schüssel geben, die Zucker-Eigelbmasse heiß und kalt
schlagen. 
Weiterrühren bis sich eine hellgelbe schaumige Masse ergibt.
In diese hellgelbe Masse die geschmolzene Schokolade einrühren.
(Die Schokolade sollte nur noch so warm sein, daß es am eingetauchten Finger
nicht schmerzt.)
Das ergibt eine zähe, tief dunkelbraune Masse. 

Sofort weiterverarbeiten, sonst erstarrt diese Masse und es gibt in der
fertigen Mousse Klümpchen!
Mit dem Schneebesen (ab jetzt nicht mehr mit dem Küchenmixer arbeiten!) wird
jetzt zuerst das Eiweiß unter die Masse gehoben und danach die Sahne.

Die fertige Masse sollte hellbraun und sehr schaumig sein.
Sie ist zuerst noch flüssig und kann in andere Gefäße umgegossen werden.
Sie wird im Kühlschrank recht schnell fest.
Verzehrfertig ist die Masse nach ca. 4-6 Stunden Kühlzeit im Kühlschrank.

Alternative:

- Für ein Schoko-Mohn-Mousse:
  Die Schokolade durch Weiße Schokolade ersetzen und Mohn unterrühren.
