# Sommertiramisu

*Dessert, Vegetarisch*

**?? Portionen**


----
- *250 g* Mascarpone
- *400 g* Naturjoghurt
- *1 Pck.* Vanillezucker
- *1 Pck.* Löffelbiscuits
- *1 Glas* rote Grütze oder Erdbeeren
- *100 g* gehobelte Mandeln
- *250 g* Quark
- *100 g* Zucker
- *250 g* Sahne
-  Butter
-  Orangensaft

----
Mascarpone, Quark, Naturjoghurt und Zucker miteinander verrühren, 
geschlagene Sahne unterheben. 
Löffelbiscuits in eine flache Auflaufform geben und mit Orangensaft beträufeln. 
1/3 der Creme darauf geben und dann die Erdbeeren darüber verteilen. 
Die restliche Creme dazu geben und alles gut kühlen. 
Die Mandeln mit 2 EL Zucker in etwas Butter anbräunen 
und ca. 20 Minuten vor dem Verzehr über die Creme streuen.
