# Aubergine in Polenta paniert

Gebratener Teil eines Hauptgerichtes

*Vegetarisch, Gebraten*


**4 Portionen**

----

- *1* Aubergine
- Polenta
- Ei
- Weizenmehl 405
- Salz
- Pfeffer
- Wasser
- Öl zum Braten

----

Aubergine in ca. 1cm dicke Scheiben schneiden.

Wasser, Mehl und Polenta in einen tiefen Teller geben.
Ei auf einen tiefen Teller geben und aufschlagen.
Mehl, Ei und Polenta mit Pfeffer und Salz würzen.

Die Scheiben erst in Wasser anfeuchten,
dann bemehlen,
anschließend mit Ei benetzen,
zuletzt beide Seiten in Polenta tunken.

Die panierten Scheiben nun im heißen Fett braten.

Varianten:

- Macht man die Scheiben wesentlich dünner,
  so bleiben die innen nicht *schlonzig*,
  sondern werden insgesamt eher knusprig.
