# Sahnige Pilzpfanne

*Gebratenes, Vegan*

**1 Portion**

----

- *500 g* Pilze
- *100 ml* Sojasahne
- Öl zum Anbraten
- Thymian
- Salbei
- Salz
- Schwarzer Pfeffer

----

Stil der Pilze entfernen und Pilze reinigen.
Köpfe vierteln, trockenen Ansatz der Stile abschneiden.
Pilze im Öl heiß anbraten, bis sie gleichmäßig braun sind aber
noch konsistenz haben.
Mit Sojasahne ablöschen.

Mit den Gewürzen abschmecken und nach belieben etwas reduzieren
lassen.

