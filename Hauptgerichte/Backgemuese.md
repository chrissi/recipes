# Backgemüse

*Hauptgerichte, Beilagen, Vegetarisch*

**2 Personen**

----

- *1* Süßkartoffel
- *1* große Kartoffel
- *250g* Feta 
- *1* große rote Paprika 
- *250g* Dose süßer Mais 
- Pfeffer
- Salz
- Ölivenöl

----

Alle Zutaten in kleine Würfel schneiden.
Die Würfel sollten dabei so klein sein, dass man später mehrere davon
gleichzeitig auf eine Gabel bekommt.

Zutaten in eine Auflaufform geben, mit etwas Öl, Pfeffer und Salz
bestreuen und vermengen.
Anschließend bei 175°C bei Ober-/Unterhitze ca. 45 Minuten backen
bis Käse und Gemüse eine angenehme Bräune erreicht haben.
