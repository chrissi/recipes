# Brühefondue (Grundrezept)

*Hauptgerichte*

**4 Personen**

----

## Brühe

- *1* Pastinake
- *2* Möhren
- *2* Zwiebeln
- *1* Lauch
- *1/4* Knollensellerie
- *1* Lorbeerblatt
- *4* Wacholderbeeren
- *1/2* Saft von Zitrone
- Salz, Pfeffer
- Rauchsalz

## Zum Kochen

- *2* Paprika
- Frische Sprossen
- *2* Pastinaken
- *2* Mittlere Kartoffeln
- *500 g* Champignons
- *2* Mittlere Zuccini
- *1/2* Kopf frischer Broccoli
- *250 g* Hühnchenbrust

-----

Aus den Brühezutaten eine klare Brühe kochen.
Dazu darauf achten, dass die Zutaten beim Anbraten nicht dunkel werden.
Die Brühe zum Schluss mit Salz, Pfeffer, Zitronensaft und etwas
Rauchsalz abschmecken.

Für das Fondue die Zutaten in sinnvoll kochbare Stücke
schneiden.
Die Kartoffeln vorkochen; die müssen aber noch nicht völlig weich sein.
