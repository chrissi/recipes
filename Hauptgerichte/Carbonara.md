# Spaghetti Carbonara

*Hauptgericht, Ei, Nudeln, Vegetarisch*

**4 Personen**

----

- *500 g* Spaghetti nach Wahl
- *3* Zehen Knoblauch
- *30 ml* Olivenöl oder Butter
- *110 g* Italienischer Hartkäse nach Wahl
- *6* Eigelb von L-Eiern
- Salz, Pfeffer

----

Hartkäse fein reiben.
Eier trennen.
Knoblauch schälen und in dünne Scheiben schneiden.

Olivenöl in eine Pfanne geben, Knoblauch hinein legen.
Bei kleiner Hitze ein Knoblauchöl herstellen.
Der Knoblauch darf gern lange im Öl ziehen ohne dabei dunkel zu werden.
(Dunkler Knoblauch würde einen bitteren Geschmack beitragen, 
den wir nicht haben wollen.)

Spaghetti nach Packungsanleitung kochen.

Geriebenen Hartkäse und Eigelbe vermengen.
Esslöffelweise (gesalzenes) Nudelwasser hinzugeben und umrühren,
bis die Masse so flüssig ist, dass man sie fast vollständig aus 
der Schüssel kippen könnte.
Mit Salz und Pfeffer abschmecken.

Kurz bevor die Nudeln fertig sind den Knoblauch aus dem Öl nehmen und wegwerfen.

Sind die Nudeln fertig kommt es auf die richtige Temperatur an:
Serviert soll das Gericht noch warm sein.
Aber das Ei soll so gerade nicht stocken.
Ich gehe dabei wie folgt vor:

Nudeln in ein Sieb geben und kurz ablaufen lassen.
Die Nudeln dürfen noch etwas Wasser haben, wenn man sie zurück in den Topf gibt.
Direkt das Knoblauchöl hinzugeben und mit einem Nudellöffel unterrühren.
Wenn man die Nudeln so vermengt ist die richtige Temperatur erreicht,
wenn die Nudeln nur noch etwas dampfen.
Dann die Ei-Käse-Masse hinzugeben und ebenfalls unterrühren.

Direkt servieren.
