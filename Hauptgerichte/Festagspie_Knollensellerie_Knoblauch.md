# Festagspie mit Knollensellerie und süßem Knoblauch

*Hauptgerichte, Knoblauch, Gebacken*

**8 Personen**

----

## Teig

- *250 g* Dinkelmehl
- *125 g* kalte Butter
- *1/2 TL* Salz
- *1* Zitrone, Zeste davon
- *25 g* geriebener Cheddar
- *1* Eigelb
- kaltes Wasser
- Rosmarin, Thymian, Salbei

## Füllung

- *3* Knollen Knoblauch
- *1 EL* Olivenöl
- *1 TL* Balsamico
- *1 EL* Honig
- *100 ml* Wasser
- *700 g* Knollensellerie
- *220 g* geriebener Cheddar
- *150 g* Crème fraîche
- *1/2*Zitrone, Saft davon
- *1 EL* Senf
- *1 Bund* Petersilie
- Worchestershiresauce
- *2* Eier
- Rosmarin, Thymian, Salbei
- Salz, Pfeffer

## Abdeckung

- *300 g* Knollensellerie
- *100 g* Cheddar
- Olivenöl
----

## Der Teig

Die Butter in kleine Stücke schneiden.
Mit dem Mehl und dem Salz in eine Schüssel geben und mit den Händen zu feinen
Streuseln vermengen.
Kräuter, Zitronenzeste, und Cheddar unterrühren.

Das Eigelb mit 1 EL kaltem Wasser aufschlagen und unter den Teil rühren.
Vorsichtig kaltes Wasser zugeben, bis ein geschmeidiger Teig entsteht.
Den Teig kaltstellen.

## Die Füllung

Knoblauchknollen in Zehen teilen und schälen.
Zehen in einen Topf geben und mit Wasser bedecken.
Erwärmen und ein paar Minuten kochen lassen.
Wasser abgießen.

Olivenöl zum Knoblauch geben und anbraten, bis der Knoblauch Farbe bekommt.
Mit Balsamico und Wasser ablöschen.
Aufkochen und bei kleiner Hitze 10 Minuten kochen lassen.
Danach Honig, und Gewürze und Salz zugeben.
Noch einmal ca 5 Minuten köchen lassen, bis das Wasser 
fast vollständig verdampft ist.
Die Knoblauchzehen sind jetzt von einem dunklen Sirup überzogen.

*700 g* Knollensellerie in gabelgereche Stücke würfeln.
In einem Topf mit Wasser bedecken.
Circa 10 Minuten kochen, bis der Knollensellerie gar ist.

Den Knollensellerie abgießen.
Käse, Crème Fraîche, Zitronensaft, Senf, Petersilie,
Worchestershiresauce und die Eier dazu geben.
Mit Salz und Pfeffer würzen.
Zuletzt die Knoblauchzehen unterheben, sodass die dabei nicht
kaputt gehen.

## Anrichten und Backen

Backofen auf 180°C (Ober- / Unterhitze) vorheizen.
Teig in eine Springform geben und auf dem Boden verteilen.
Am Rand hochziehen.
Die Füllung auf den Teig geben.

Den Cheddar auf die Füllung geben.
Die *300 g* Knollensellerie grob raspeln und
auf den Cheddar geben.
Mit Olivenöl beträufeln.

Circa 45 Minuten backen.
Den Pie danach kurz abkühlen lassen und war servieren.
