# Vegane Graupensuppe

*Hauptgerichte, Eintopf, vegan*

**8 Personen**

----

## Graupen

- *200 g* Perlgraupen, mittlere Größe
- Salz

## Suppe

- *400 g* Zwiebeln
- *1/3* Knollensellerie
- *400 g* Möhren
- *500 g* Kartoffeln
- *2 Stangen* Lauch
- *125 g* Vegane Butter
- *1/2 TL* Rauchsalz
- *3 Blatt* Lorbeer
- *4 Stück* Wacholderbeeren
- *1 TL* Worcestersauce
- *1 TL* Sojasauce
- Muskatnuss, Pfeffer, Salz, Kerbel, Kreuzkümmel
- Wasser

----

Graupen in einem separaten Topf mit etwas Salz in viel Wasser kochen.
Anschließend in ein Sieb geben und mit viel kaltem Wasser so lange spülen,
bis keine Stärke mehr ausgewaschen wird.
Beiseite stellen.

Zwiebeln, Knollensellerie, Möhren, Lauch und Kartoffeln in löffelgerechte
Stücke schneiden.

Butter auslassen.
Zwiebeln und Sellerie darin anbraten, bis sie Farbe ansetzen.
Möhren und Lauch kurz mitbraten.
Mit Wasser ablöschen.

Kartoffeln dazugeben.
Wasser nachgießen, bis die Masse bedeckt sind.
Grob abschmecken.
Worcestersauce, Sojasauce und Wacholderbeeren und Lorbeer hinzugeben.
Aufkochen und 30 Minuten leise köcheln lassen.

Danach die Graupen dazugeben.
Eventuell Wasser nachgießen, bis die Masse wieder gut bedeckt ist.
Eventuell neu abschmecken.

Über Nacht kühl stehen lassen.
Am nächsten Tag erwärmen, mit Wasser auf die gewünschte Konsistenz
bringen und wieder abschmecken.