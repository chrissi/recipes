# Grünkohlpizza

**Hauptgerichte, Fleisch**

*3 Personen*

----

## Grünkohl-Belag

- *1 Glas* küchenfertiger Grünkohl
- *1 Packung* Mettenden
- *1* Zwiebel
- *2 EL* Butterschmalz
- etwas Parmesan
- Rauchsalz
- Pfeffer
- Salz

## Pizza Grundzutaten

- *2* Pizzateiglinge
- Pizza-Tomatensauce
- *300g* Streukäse

----

## Grünkohl-Belag

Zwiebel fein hacken und in Butterschmalz auslassen.
Grünkohl aus dem Glas dazu geben.
Dabei die Flüssigkeit mitnehmen.
Mettenden dazu geben.
Alles gemeinsam mindestens 30 Minuten köcheln lassen.

Danach mit Rauchsalz, Pfeffer und Salz abschmecken.
Hier kann man ein wenig (und wirklich nur ein wenig)
Parmesan für einen kräftigeren Geschmack dazu geben.

Am Ende sollte der Grünkohl noch viel Flüssigkeit haben,
da er sonst im Backofen trocken wird.

## Pizza Stacking

Grundpizza mit Teig, Tomatensauce und Käse vorbereiten.
Mettenden aus dem Grünkohl nehmen und in Scheiben schneiden.
Grünkohl auf der Pizza verteilen.
Mit Mettendenscheiben belegen.

Wie gewohnt backen.
