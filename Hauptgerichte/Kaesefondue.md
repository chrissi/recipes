# Käsefondue

*Hauptgericht, vegetarisch*

**2 Portionen**

----

- *400 g* Kräftiger Käse (z.B. Greyerzer)
- *200 ml* Trockener Weißwein
- *2 Stück* Knoblauchzehen
- Speisestärke
- Muskatnuss
- *200 g* Helles Weizenbrot zum Dippen

----

Käse grob reiben und mit etwas Wein und den Knoblauchzehen
im Fonduetopf schmelzen.
Nach dem Schmelzen Weißwein und Speisestärke zugeben,
bis man die richtige Konsistenz hat.

Mit Muskatnuss würzen.
In der Regel braucht man kein Salz oder Pfeffer zugeben.

