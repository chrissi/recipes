# Vegane Kartoffelbowl

*Hauptgericht, vegan*

**2 Personen**

----

## Kartoffelbasis

- *80 g* Lauch
- *130 g* Zwiebeln
- *100 g* Sellerie
- *100 g* Möhren
- *700 g* Kartoffeln (egal ob festkochend oder mehligkochend)
- *60 g* vegane Butter (z.B. Alsan)
- *100 g* Vegane Crème fraîche
- Wasser
- Bratöl
- Salz
- Pfeffer

## Topping: Süßkartoffel

- *300 g* Süßkartoffel
- *1 EL* Zwiebelpulver
- *2 EL* Paprikapulver (süß)

## Topping: Champignons

- *200 g* Champignons
- *1 EL* getrockneter Salbei
- *1 EL* Majoran

## Topping: Kürbis

- *200 g* Hokkaiokürbis
- *2 EL* Zuckerrübenrisup

## Topping: Paprika
- *1* Paprika

## Topping: Frühlingszwiebeln

- *0.5 Bund* Frühlingszwiebeln

## Topping: Hack

- *50 g* Hack
- *50 g* veganes Hack

## Topping: Röstzwiebeln

- *50 g* Röstzwiebeln

----

## Kartoffelbasis

- Gemüsezutaten schälen, wo nötig
- Gemüsezutaten grob würfeln
- Lauch, Zwiebeln, Sellerie und Möhren glasig braten
- Kartoffeln dazu geben und etwas weiter braten
- Wasser zugeben, bis alles gerade bedeckt ist
- Salzen
- Kochen, bis die Kartoffeln gar sind
- Pürieren
- Butter und Crème fraîche zugeben und abschmecken

## Toppings

- Zutaten werden idR. in der Pfanne gebraten und gewürzt.
- Dann lauwarm zur Kartoffelbasis gereicht.