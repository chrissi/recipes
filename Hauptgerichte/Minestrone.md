# Minestrone

*Suppe, Fleisch, Hauptgericht*

**4 Portionen**

----

- *1 Stück* durchwachsener, geräucherter Bauchspeck
- Butter zum Anbraten
- *4* große Zwiebeln
- *4 Stangen* Staudensellerie
- *2* große Möhren
- *4* große Kartoffeln
- *150 g* Brechbohnen (TK)
- *400 g* Zucchini
- *4 Zehen* Knoblauch
- *1 l* Gemüsebrühe
- *100 g* Tomaten
- *100 g* Parmesanrinde
- *50 ml* Olivenöl
- Salz
- Pfeffer

----

Eventuelle Schwate vom Bauchspeck abschneiden.
Den restlichen Bauchspeck würfeln.
Bauchspeck und Schwate in Butter auslassen.

Zwiebeln, Staudensellerie und Möhren fein schneiden und im Fett anbraten.

Kartoffeln, Zucchini, Knoblauch und wenn nötig die Bohnen schneiden und mit anbraten.

Mit der Brühe ablöschen.
Tomaten, Parmesanrinde und Olivenöl dazu geben.
Einmal aufkochen.
Anschließend ca. 1 Stunde köcheln lassen.

Mit Salz und Pfeffer abschmecken.
