# Möhren-Miso-Suppe

*Hauptgericht, vegan*

**2 Personen**

----

- *500 g* Möhren
- *1* Zwiebel
- *2 Zehen* Knoblauch
- *20 g* Ingwer
- *750 ml* Gemüsebrühe
- *3 TL* Miso Paste
- *etwas* Limetten- oder Zitronensaft
- Salz, Pfeffer
- Chilli oder Chillipulver
- Kokosmilch
- Öl zum Braten

----

Möhren, Zwiebel, Knoblauch, Ingwer und Chilli klein schneiden.
Etwas Öl erhitzen und geschnittene Zutaten leicht anbräunen.
Mit der Brühe ablöschen und 20 Minuten kochen lassen.
Danach pürieren.
Misopaste und Limettensaft hinzugeben.
Mit Salz, Pfeffer und Chilli abschmecken.

In einer Schüssel servieren und in der Mitte etwas Kokosmilch hineingeben.
