# Moussaka

*Hauptgerichte, Gratiniert*

**5 Portionen**

----

## Füllung

* *800 g* Auberginen
* *800 g* Kartoffeln
* Salz
* Öl zum Braten

## Tomatenmasse

* *FIXME g* Sojagranulat
* Sojasauce
* Worcestersauce
* *3 Stück* mittlere Zwiebeln
* *5 Stück* Knoblauchzehen
* *2 Dosen* gehackte Tomaten
* *1 Stück* Lorbeerblatt
* Salz
* Pfeffer
* Zucker
* Zimt
* Muskatnuss

## Bechamel-Sauce

* *40 g* vegane Butter
* *40 g* Mehl
* *375 ml* Milch
* Salz
* Pfeffer
* Muskatnuss

## Topping

* Olivenöl
* *50 g* Hartkässe
* *150 g* Streukäse

----

## Füllung

* Auberginen in 2 mm dicke Scheiben schneiden, salzen und mind. 30 Minuten stehen lassen.
  Anschließend trockentupfen und von jeder Seite ohne Öl ca. 2 Minuten braten.
  Die Auberginen sollen etwas Farbe bekommen aber nicht zu dunkel werden.
* Kartoffeln schälen, in 2 mm dicke scheiben schneiden.
* Kartoffelscheiben in Öl ca. 10 Minuten braten.
  Dabei salzen.
  Die Scheiben dürfen dabei Farbe bekommen, müssen aber nicht knusprig sein.
  Anschließend zur Seite stellen.

## Tomatenmasse

* Soja-Granulat mit Wasser aufgießen, dabei mir Sojasauce und Worcestersauce würzen.
  Ein paar Minuten stehen lassen, bis das Granulat das Wasser aufgenommen hat.
* Zwiebeln fein würfen.
* Zwiebeln zusammen mit dem Soja-Granulat bei mittlerer Hitze braten, bis alles etwas
  Farbe angesetzt hat.
* Knoblauch schälen. Kurz vor Ende des Bratens dazu geben.
* Mit den gehackten Tomaten ablöschen, dann aufkochen.
* Mit Salz, Pfeffer, Zimt undMuskatnuss abschmecken.
  Lorbeerblatt dazu geben.
* Masse köcheln lassen, bis sie kräftig eingedickt ist.

## Bechamel-Sauce

* Mehlschwitze herstellen.
* Mit Milch aufgießen.
* Abschmecken.

## Backen

* Olivenöl in die Auflaufform geben.
* Eine Lage erst Kartoffeln und dann Auberginen stapeln.
* Danach die Tomatenmasse darauf geben.
* Mit geriebenem Hartkäse bestreuen.
* Anschließend eine Lage aus den restlichen Kartoffeln und Auberginen staplen.
* Bechamel darüber geben.
* Mit dem Streukäse bestreuen.
* Bei 180 °C ca 30 .. 45 Minuten backen.