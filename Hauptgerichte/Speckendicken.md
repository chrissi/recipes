# Speckendicken

*Hauptgerichte, Fleisch*

**4 Personen**

----

## Teig

- *300 g* Weizenmehl 1050
- *600 g* Roggenschrot
- *200 g* Zuckerrübensirup
- *3* Eier
- Milch
- Salz, Pfeffer
- Anis, Kardamom nach Belieben

## Zum Braten

- *250 g* Mettenden
- *250 g* Gewürfelter Schinken
- Etwas Öl

----

Frei nach diesem [Rezept](https://www.botschaft-ostfriesland.de/fetter-start-ins-neue-jahr-speckendicken-selber-braten/)


## Teig zubereiten

Milch erwärmen, mit dem Zuckerrübensirup
und den Eiern und den Gewürzen verrühren.
Nach und nach das Mehl zugeben bis eine
zähe Masse entsteht.

Den Teig einige Stunden ruhen lassen.
Danach noch etwas Milch zugeben, falls er zu
fest geworden ist.

## Ausbacken

Die Mettenden in Scheiben schneiden.

Vor dem ersten Backen das Waffeleisen fetten.

In einem normalen Waffeleisen etwas Speck und 
ein paar Scheiben Mettenden anbraten.
Dann Teig dazu geben und oben noch etwas Speck und ein paar Scheiben Mettenden dazu geben.
Die Waffel ausbacken.
