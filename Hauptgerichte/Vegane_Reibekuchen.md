# Vegane Reibekuchen

*Kartoffel, vegan*

**8 Personen**

----

- *3,5 kg* Kartoffeln, festkochend
- *3* Zwiebeln
- *2 EL* Mehl
- Salz, Pfeffer, Muskat
- *500 ml* Öl zum Ausbraten

----

Kartoffeln und Zwiebeln schälen.

2/3 der Kartoffeln und Zwiebeln mit der groben Reibscheibe stiften.
1/3 der Kartoffeln und Zwiebeln mit der feinen Reibscheibe reiben.

In eine große Schüssel geben und kräftig salzen.
10 Minuten ziehen lassen.
Danach die Masse in ein Trockentuch geben und das Wasser kräftig ausdrücken.

Mehl zugeben.
Masse mit Salz, Pfeffer und Muskat abschmecken.
Gut vermengen.

Zum Backen jeweils einen Kleks in das heiße Fett geben und mit dem Pfannenwender
platt drücken.
Backen, bis die Außenseiten schön knusprig sind.
Wenig wenden, da die Reibekuchen erst durch das Braten strukturele Integrität bekommen.
