# Veganer Grünkohl

*Hauptgerichte, Eintopf, vegan*

**15 Personen**

----

## Grünkohl

- *3 kg* Grünkohl (z.B. aus der Dose, grob gehackt, nicht servierfertig (gesalzen ist ok))
- *1600 g* Zwiebeln
- *150 g* Vegane Butter (z.B. Alsan-S)
- Öl zum Anbraten
- *500 g* Räuchertofo
- Rauchsalz (eventuell)
- Gemüsebrühe (glutenfrei)
- Salz, Pfeffer

## Kartoffeln

- *4 kg* Kartoffeln
- Salz

## Servieren

- Senf

---

## Grünkohl

Zwiebeln grob würfen.
Vegane Butter auslassen.
Zwiebeln darin glasig dünsten.

Grünkohl incl. Flüssigkeit dazu geben.
Eventuell Wasser zugeben.
Mit Salz, Pfeffer und Gemüsebrühe grob abschmecken.
Aufkochen und 30 Minuten köcheln lassen.

Räuchertofo in schinkenwürfelgroße Würfel schneiden.
In Öl anbraten, bis diese außen leicht knusprig werden.
Zum Grünkohl geben und mitkochen.

Abschließend noch einmal abschmecken.
Bei Bedarf den Rauchgeschmack mit dem Rauchsalz verstärken.

## Kartoffeln

Schälen, eventuell kleiner schneiden und wie gewohnt kochen.

## Servieren

Beim Servieren Senf dazu reichen.