# Mac-n-Cheese - it's that simple

*Hauptgerichte, Vegeratisch, Feel Good*

**3 Personen**

----

## Nudeln 
- *500 g* Nudeln, kleine Macaroni oder Penne
- Salz

## Sauce
- *150 g* Cheddar, gerieben
- *200 g* Schmelzkäse
- *100 ml* Milch
- *100 ml* Weißwein
- *2 EL* Mehl
- *50 g* Butter
- Muskatnuss, Pfeffer

## Überbacken
- *50 g* Cheddar, gerieben

----

Nudeln wie gewohnt kochen.

Aus Butter und Mehl eine Mehlschwitze herstellen.
Mit Milch und Weißwein ablöschen.
Schmelzkäse und Cheddar in der Sauce auflösen.
Mit Muskatnuss und Pfeffer abschmecken.
Die Sauce zu einer glatten Masse rühren.

Nudeln in eine Auflaufform geben, Sauce darüber geben
und vermengen.
Anschließend mit Käse bestreuen.

Bei 200 °C Umluft und Grillstab überbacken bis der Käse Farbe bekommt.