# Vegane Linsensuppe

*Hauptgerichte, Eintopf, vegan*

**5 Personen**

----

- *250 g* Räuchertofu
- *4* Zwiebeln
- *4* Möhren
- *4* Kartoffeln
- *250 g* Tellerlinsen
- *1/2 Tube* Tomatenmark
- *1 l* Wasser
- Öl zum Anbraten
- Gemüsebrühe, Salz
- Balsamico
- eventuell Räuchersalz

----

Räuchertofu, Zwiebeln, Möhren und Kartoffeln in Würfel schneiden.

Zwiebeln und Räuchertofu anbraten.
Später Möhren und Kartoffeln und Tomatenmark dazu geben.
Wenn die Masse etwas Farbe bekommen hat mit Wasser aufgießen.
Tellerlinsen, Gemüsebrühe, Salz und eventuell Räuchersalz hinzugeben.

Aufkochen und anschließend circa 60 Minuten leicht köcheln lassen.
Zuletzt mit Balsamico, Gemüsebrühe und Rauchsalz abschmecken.