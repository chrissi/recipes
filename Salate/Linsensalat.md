# Linsensalat

*Salat, Vegan*

**4 Portionen**


----

- *150 g* rote Linsen
- *150 g* Berglinsen
- *1* rote Paprika
- *1* gelbe Paprika
- *2* mittelgroße Zwiebeln
- Petersilie
- *1* Knoblauchzehe
- Salz, Pfeffer
- Kreuzkümmel
- etwas Zucker
- *2 EL* weißer Balsamico-Essig
- *4 EL* Olivenöl
- *2 TL* Senf
- *1 EL* Bratöl

----
Die Linsen nach Packungsanleitung garen.
Die Paprika und Zwiebeln fein hacken.

Die Zwiebel in etwas Öl glasig dünsten und mit dem Zucker karamellisieren.

Linsen, Paprika Zwiebel und Petersilie vermengen.
Den Knoblauch pressen und dazu geben.

Aus dem Olivenöl, Essig und Senf eine Vinigrette anrühren und mit Salz, Pfeffer
und Kreuzkümmel würzen.
Die Vinigrette über die Linsen geben und alles gut vermengen.

Den Salat mindestens über Nacht ziehen lassen.
