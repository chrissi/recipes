# Spanischer Zitronen-Kartoffelsalat

*Salat, vegan*

**4 Portionen**


----
- *750 g* fest kochende Kartoffeln
- Salz
- *2* Zitronen
- *1* Knoblauchzehen
- *1 EL* Weißweinessig
- *4 EL* Gemüsefond
- *6 EL* Olivenöl extra nativ
- schwarzer Pfeffer
- *2 EL* gehackte Petersilie 

----

Die Kartoffeln waschen und in Salzwasser zugedeckt 25-30 Min. kochen. 
Abgießen, ausdampfen lassen und pellen. 
Noch lauwarm längs halbieren und in Scheiben schneiden. 
Die Zitronen auspressen.
Den Knoblauch abziehen und fein würfeln. 
Zitronensaft, Knoblauch, Essig und Gemüsefond verrühren. 
Das Öl unterquirlen, mit Salz und Pfeffer abschmecken. 
Die Sauce über die Kartoffeln gießen, vorsichtig vermischen. 
Mit Petersilie bestreuen und servieren.

