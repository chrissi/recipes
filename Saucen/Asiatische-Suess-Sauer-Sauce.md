# Asiatische Sauce süß-sauer

Eine tomatige süß-saure Soße zu Reis oder gebratenen Asia-Nudeln

*Sauce, Vegan, Vorrat*

**?? Portionen**


----

- *6 EL* Essig
- *6 EL* Zucker 
- *1 TL* Salz 
- *2 EL* Tomatenmark 
- *2 EL* Sojasauce 
- *2 TL* Öl (Sesamöl) 
- *2 EL* Speisestärke 
- *1 Dose* Ananas in Scheiben 
- *1*  Paprikaschote(n), grün 

----
Zuerst Ananas abtropfen lassen. Den Saft auffangen, er wird später noch benötigt.
Die Ananasringe in Stücke schneiden. 
Die Paprika waschen, entkernen und in Würfel schneiden.

Dann Essig, Zucker, Salz, Tomatenmark, Sojasoße und Sesamöl in einen Topf geben
und erhitzen. 
Die Speisestärke zuerst eine einer Schale mit ein wenig Wasser anrühren,
bis alle Klümpchen weg sind, dann zu der Mischung in den Topf geben. 
Wenn die Bindung der Soße anzieht, mit Ananassaft auffüllen. 

Dann die Ananasstücke und die grüne Paprika dazugeben und bei mittlerer
Temperatur mitköcheln. 
Gelegentlich umrühren, damit die Soße nicht anbrennt. 
Je nach gewünschter Konsistenz und Süße noch etwas Ananassaft oder Wasser
hinzufügen. 
