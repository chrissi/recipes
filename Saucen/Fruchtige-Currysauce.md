# Fruchtige Currysauce

Sauce für Currywurst

*Sauce, Vegan*

**?? Portionen**


----

- *1 Dose* Tomatenmark, (125 g) 
- *2 EL* Currypulver 
- *1 EL* Cayennepfeffer 
- *250 ml* Orangensaft   
- Salz und Pfeffer 
- *1 Prise* Zucker 
- *125 ml* Brühe, gekörnte 
- *2* Sternanis 
- *1 EL* Olivenöl 

----
Tomatenmark in Olivenöl anschwitzen.
Currypulver und Cayennepfeffer dazugeben und mitschwitzen, 
anschließend mit Orangensaft ablöschen. 
Gekörnte Brühe einrühren und alles aufkochen. 
Mit Salz, Pfeffer und Sternanis abschmecken und etwas köcheln lassen. 
