# Fruchtige Dunkle Sauce

Eine Dunkle Sauce zu Gebratenem, Kartoffeln und solchen Dingen.

*Sauce, Vegan, Vorrat*

**15 Portionen**


----

- Öl zum Anbraten
- *500 g* Knollenselerie
- *300 g* Möhren
- *400 g* Rote Zwiebeln
- *300 g* Tomaten
- *0.5* Knoblauchknolle
- *1* Paprika
- *0.5 l* Glühwein
- *0.2 l* Sherry
- *20 g* Salz
- *12* Pfefferkörner schwarz
- *10* Pfefferkörner rot
- *5* Wacholderbeeren
- *2* Lorbeerblätter
- *90 g* Weizenmehl 405
- *70 g* Öl
- *50g* Tomatenmark

----
Bei dieser Sauce gilt: Die Farbe soll durch das Anbraten und Karamelisieren
entstehen.
Man darf sich beim Anbraten und Rösten also durchaus Zeit lassen.
Sollte es zwischendurch zu sehr ansetzen, darf man das Gemüse gern schon einmal
mit Glühwein oder Sherry ablöschen und den Topfboden damit *freikochen*.

Knollenselerie und Möhren in grobe würfel Schneiden und im Öl in einem großen
Topf anbraten bis Farbe entsteht.
Die Zwiebeln und die Paprika ebenfalls grob schneiden und nach einiger Zeit
zum Wurzelgemüse dazu geben.
Zwischendurch ruhig mit Sherry oder Glühwein ablöschen.
Anschließend weiter braten.

Die Tomaten vierteln (und wenn man mag den Stielansatz entfernen).
Knoblauch schälen und in grobe Stücke schneiden.
Wenn man mit dem Anbraten fertig ist den restlichen Glühwein und Sherry,
die Tomaten, den Knoblauch und die Gewürze dazu geben.
Die Sauce gut aufkochen und danach noch mal 45 Minuten köcheln lassen.

Zum Schluss die Sauce passieren bzw. durch ein Sieb gießen.

Mit dem Öl und dem Weizenmehl eine Mehlschwitze herstellen.
Diese darf man gern dunkel werden lassen.
Das Tomatenmark mit zur Mehlschwitze rühren.
Das ganze darf noch ein wenig rösten.

Zum Schluss die Mehlschwitze in die Sauce rühren und das ganze noch einmal
aufkochen. 
Danach abschmecken.


Erfahrungen:

- Beim ersten Kochen hatte ich eher *100 g* Tomatenmark verwendet.
  Damit war die Sauce sehr tomatig.
  Das war nicht schlimm; war aber auch nicht nötig.
