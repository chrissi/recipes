# Fruchtige Wintersauce

Eine fruchtige Sauce zu Rotkohl und Klößen

*Sauce, Vegetarisch*

**4 Portionen**


----

- *1 St* Orange
- *5 St* Backpflaumen - die weichen
- *2 EL* Sultaninen
- etwas Puderzucker
- etwas Salz und Pfeffer
- etwas Rosmarin
- *250 ml* Rotwein
- *1 EL* Preiselbeeren oder Johannisbeergelee

----

Die Orange schälen und filetieren, zusammen mit den kleingeschnittenen
Backpflaumen und den Sultaninen in etwas Butter andünsten, dabei mit
Puderzucker bestäuben und karamellisieren lassen.

Mit Rotwein ablöschen, salzen, pfeffern und etwas Rosmarin dazu geben.
Durchkochen lassen. 
Mit einem ordentlichen Löffel Preiselbeeren (oder Johannisbeergelee) abrunden. 
