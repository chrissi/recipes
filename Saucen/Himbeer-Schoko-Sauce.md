# Himbeer-Schoko Sauce

Eine fruchtige Dessertsoße zu Eis, Pudding, Panacotta

*Sauce, Süß, Vegetarisch, Vorrat*

**?? Portionen**


----

- *450 g* TK-Himbeeren
- *1* Zitrone
- *150 g* Puderzucker
- *1 EL* Himbeersirup
- *75 g* Zartbitterschokolade
- *1 EL* Portwein (nach Belieben)

----
Die Gläser (am besten Twist-off- oder Bügelgläser) mit heißem Wasser waschen
und umgedreht auf einem Küchentuch trocknen lassen. 

Die unaufgetauten Himbeeren in einen Topf geben. 
Die Zitrone auspressen und den Saft mit Puderzucker und dem Himbeersirup
dazugeben. 
Alles bei mittlerer Hitze aufkochen und ca. 5 Min. köcheln lassen.
Dann den Topf vom Herd nehmen und die Himbeeren etwas abkühlen lassen. 
Die Himbeermischung durch ein feines Sieb in einen Topf streichen. 

Die Schokolade in Stücke brechen, dazugeben und bei mittlerer Hitze unter
Rühren schmelzen lassen. 

Nach Belieben den Portwein dazugeben und unterrühren. 
Die Himbeersauce in Gläser füllen. 
Die Gläser sofort verschließen und in den Kühlschrank stellen. 

Hält sich 2-3 Wochen.
