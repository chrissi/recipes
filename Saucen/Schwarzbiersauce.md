# Schwarzbiersauce

Kräftige Sauce zu schwerem Essen

*Sauce, Fleisch*

**10 Portionen**

----

- *1 Glas* Rinderfond
- *50 g* Butterschmalz
- *1 EL* Weizenmehl 405
- *0.33 l* Schwarzbier
- *2 EL* Pflaumenkompott
- Salz
- Roter Pfeffer
- Paprika Edelsüß
- Zucker

----

Aus Butterschmalz und Weizenmehl eine dunkle
Mehlschwitze herstellen.
Mit Rinderfond und Schwarzbier ablöschen.
Mit Pflaumenkompott und Gewürzen abschmecken.


