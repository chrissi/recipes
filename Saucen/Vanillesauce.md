# Vanillesauce

Eine Dessertsoße zu Pudding, Eis oder Bratapfel

*Sauce, Vegetarisch, Süß*

**4 Portionen**


----

- *3*  Eigelb
- *2 EL* Zucker
- *2 EL* Vanillezucker
- *1 EL* Stärkemehl
- *375 ml* Milch
- *1* Vanilleschote(n)

----
Das Mark aus der Vanilleschote herauskratzen.
Eigelb, Zucker, Vanillezucker und Stärkemehl glatt rühren. 
Dann die Milch unterschlagen sowie das Mark und die Schote (getrennt)
in die Milch geben. 
Nun auf kleiner Flamme schlagen, bis die Sauce dicklich wird.
Sie darf nicht kochen. 
Die Schote wieder entfernen. 

