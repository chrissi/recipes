# Zitrone-Basilikum-Sauce

*Sauce, vegetarisch*

**2 Personen**

----

- *1 EL* Butter
- *1* Bio-Zitrone
- *150 ml* Sahne
- *1 Bund* Basilikum
- *30 g* Parmesan
- Salz, Pfeffer

----

Basilikum hacken.
Schale von der Zitrone abreiben.
Den Rest der Zitrone auspressen.
Parmesan reiben.

Butter mit der Zitronenzeste und dem Saft von ca. 1/2 Zitrone leicht aufkochen.
Mit Sahne ablöschen.
Basilikum und Parmesan dazu geben.
Mit Salz und Pfeffer abschmecken.

Wenn man will kann man nun die Nudeln unterheben 😃.
