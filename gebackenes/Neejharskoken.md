# Neejahrkoken, Friesenrollen

*Gebackenes, Vegetarisch*

**25 Stück**

----

- *225 g* Weizenmehl 405
- *100 g* weißer Kandis
- *100 g* Butter
- *1* Ei
- *1* gemahlener Zimt
- *2* gemahlener Kardamom
- *250 ml* Wasser

----

Wasser kochen und über den Kandis geben.
Den Kandis im Wasser auflösen lassen.
Butter schmelzen.
Zuckerwasser abkühlen lassen.

Flüssige Butter rühren und nach und nach mit
Zuckerwasser, Ei, Gewürzen und Mehl vermengen.
Zu einem glatten Teig rühren.
Den Teig bis zum nächsten Tag kühlstellen und
erst dann backen.

Beim Backen darauf achten, dass der Teig
im Eiserkuchen-Gerät wirklich dünn ausgebacken wird.
Im Zweifel das Gerät nach einlegen des Teigs sanft
zudrücken.
Die Waffeln direkt nach dem Backen mit den Händen
rollen bzw. formen.
