# Roggenbrot

*Brot, gebackenes, vegan*

**1kg Brot**

----

## Sauerteig

- *248 g* Wasser @ 50 °C
- *5 g* Salz
- *248 g* Roggenmehl 1150
- *50g* Roggenanstellgut

## Hauptteig

- *224 g* Wasser @ 70 °C
- *9 g* Salz
- *348 g* Roggenmehl 1150
- *19 g* Flüssigmalz (inaktiv)
- Sauerteig

----

## Sauerteig

Zutaten vermengen, dabei das Anstellgut als letztes auf das Mehl geben.

Ca. 12 Stunden bei 20 .. 22 °C reifen lassen.

## Hauptteig

Zutaten vermengen, dabei den Sauerteig als letztes auf das Mehl geben.

Abdecken und 30 Minuten ruhen lassen.

## Rundwirken

Den Teigling auf eine bemehlte Arbeitsfläche legen und rundwirken.
Mit dem Schluss nach unten in einen bemehlten Gärkorb geben.
Abdecken und zwei Stunden reifen lassen.
Der Teigling sollte sich um 1/2 oder 3/4 vergrößert haben.

## Backen

Backofen mit Bedampfer auf 250 °C sehr gut vorheizen.
Teigling auf ein Backblech mit Backpapier stürzen.
Temperatur auf 220 °C reduzieren.
Sofort bedampfen.
Dampf nach 10 Minuten ablassen.
Insgesamt eine Stunde backen.

Aus und frei nach dem Plötzblog Brotkalender 2022.
