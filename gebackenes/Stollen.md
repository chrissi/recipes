# Stollen

Basierend auf [Christstollen -ultimativ-](https://www.chefkoch.de/rezepte/835961188374766/Christstollen-ultimativ.html)

*weihnachten, gebackenes*

**3x 500g Stollen**

----

## Frucht-Mischung

- *250 g* Rosinen
- *50 g* gestiftete Mandeln
- *75 g* [Orangeat]()
- *60 ml* Rum (Empfehlung: Burke's feiner alter Jamaica Rum)

## Zucker-Mischung

- *150 g* Zucker
- Orangenschalen
- Tonkabohne

## Vorteig

- *1 Würfel* Hefe
- *1 EL* Zucker
- *1 Schluck* Milch
- *1 EL* Mehl

## Teig

- *85 ml* Milch
- *250 g* Butter
- *1 Prise* Salz
- *50 g* Marzipan

## Finnish

- *250 g* Butter
- Vanillezucker
- Puderzucker
- *10 m* Alufolie

----

## Vorabend

Frucht-Mischung und Zucker-Mischung vorbereiten.
Alle weiteren Zutaten rauslegen, damit sie die gleiche Temperatur bekommen

## Vorteig

Hefe in eine Schüssel bröseln,
lauwarme Milch und Zucker untermischen,
mit Mehl bestäuben

Abdecken und 30 Minuten gehen lassen.

## Hauptteig

Das Mehl auf eine Arbeitsfläche sieben.
Butter in Flocken verteilen, Zucker und Salz dazu geben.

Die aufgegangene Hefe dazu geben.

Alles mit einem Messer bröselig hacken/vermengen,
dann den Teig (mit den Händen) kneten.

Die Milch nach und nach dazu geben und mit verkneten.

Die Früchte kurz unterkneten.

Den Teig abdecken und 30 Minuten gehen lassen.

## Backblech vorbereiten

Ofen auf 220°C Ober-/Unterhitze vorheizen.
Ein Backblech mit 3 Schichten Alufolie und Backpapier belegen.

(Den nächsten Vorteig ansetzen.)

## Stollen formen

Den Teig kurz durchkneten und in drei gleichgroße Stück teilen,
und mit einem Marzipanflöz zu Leibern formen. 

Ggf. die Rosinen in die Stollen drücken und die Löcher verschließen.

Die Stollen auf das Backblech legen, mit einem Tuch bedecken
und 30 Minuten gehen lassen.

## Backen

Stollen mit Stollenformen bedecken
und ca 30 Minuten backen.

Sofort nach dem Backen die Stollen mit flüssiger Butter bestreichen.

## Nachbereitung

Nach dem Auskühlen den Stollen nochmals mit flüssiger Butter
bestreichen, erst mit Vanillezucker und dann dick mit
Puderzucker bestreuen.

Die Stollen dick in Alufolie einpacken und mind. 3 Wochen
kühl und trocken lagern.
